'use strict';

const axios = require('axios');


function StegWs(baseUrl) {
  this.stegUrl = `${baseUrl}/api/steg/`;
  this.imagesUrl = `${baseUrl}/api/images/`;
  // this.unhideImgUrl = `${baseUrl}/api/images/steg`;
}

module.exports = StegWs;

StegWs.prototype.list = async function (q) {
  try {
    const url = this.imagesUrl + q;
    // const url = this.imagesUrl;
    const response = await axios.get(url);
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

StegWs.prototype.get = async function (id) {
  try {
    const response = await axios.get(`${this.stegUrl}/inputs/${id}`);
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

StegWs.prototype.hide = async function (hideForm) {
  try {
    const stegDetails={"outGroup":"steg","msg":hideForm.msg}
    console.log("hideForm");
    console.log(hideForm);
    const response = await axios.post(`${this.stegUrl}inputs/${hideForm.choice}`,stegDetails);
    return response;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

StegWs.prototype.unhide = async function (unhideForm) {
  try {
    const selectedImage = unhideForm.choice;
    console.log(`${this.stegUrl}steg/${selectedImage}`);
    const response = await axios.get(`${this.stegUrl}steg/${selectedImage}`);
    console.log(response.data);
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

StegWs.prototype.update = async function (user) {
  try {
    console.log(user.submit+" user.submit");
    console.log(user.fb+" user.fb");

    const response = await axios.patch(`${this.stegUrl}/${user.id}`, user);
    return response.data;
  }
  catch (err) {
    throw (err.response.data) ? err.response.data : err;
  }
};

