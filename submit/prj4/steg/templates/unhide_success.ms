<!DOCTYPE html>
<html>
  <head>
    <title>Unhide Success</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
      {{>footer}}
    <h1>Unhide Success</h1>
    <ul>
      {{#errors}}
        <li class="error">{{.}}</li>
      {{/errors}}
    </ul>
    
    {{#users}}
        <ul>
            <li>Message <b>"{{.}}"</b> retrieved</li>      
        </ul>	
	  {{/users}}

  </body>
</html>
