#!/usr/bin/env nodejs

'use strict';

const Ppm = require('./ppm');

/** prefix which always precedes actual message when message is hidden
 *  in an image.
 */
const STEG_MAGIC = "stg";

/** Constructor which takes some kind of ID and a Ppm image */
function StegModule(id, ppm) {
  this.id = id;
  this.ppm = ppm;
}

/** Hide message msg using PPM image contained in this StegModule object
 *  and return an object containing the new PPM image.
 *
 *  Specifically, this function will always return an object.  If an
 *  error occurs, then the "error" property of the return'd object
 *  will be set to a suitable error message.  If everything ok, then
 *  the "ppm" property of return'd object will be set to a Ppm image
 *  ppmOut which is derived from this.ppm with msg hidden.
 *
 *  The ppmOut image will be formed from the image contained in this
 *  StegModule object and msg as follows.
 *
 *    1.  The meta-info (header, comments, resolution, color-depth)
 *        for ppmOut is set to that of the PPM image contained in this
 *        StegModule object.
 *
 *    2.  A magicMsg is formed as the concatenation of STEG_MAGIC,
 *        msg and the NUL-character '\0'.
 *
 *    3.  The bits of the character codes of magicMsg including the
 *        terminating NUL-character are unpacked (MSB-first) into the
 *        LSB of successive pixel bytes of the ppmOut image.  Note
 *        that the pixel bytes of ppmOut should be identical to those
 *        of the image in this StegModule object except that the LSB of each
 *        pixel byte will contain the bits of magicMsg.
 *
 *  The function should detect the following errors:
 *
 *    STEG_TOO_BIG:   The provided pixelBytes array is not large enough 
 *                    to allow hiding magicMsg.
 *    STEG_MSG:       The image contained in this StegModule object may already
 *                    contain a hidden message; detected by seeing
 *                    this StegModule object's underlying image pixel bytes
 *                    starting with a hidden STEG_MAGIC string.
 *
 * Each error message must start with the above IDs (STEG_TOO_BIG, etc).
 */
StegModule.prototype.hide = function (msg) {

  let dataLen = this.ppm.width * this.ppm.height * 3/8;
  let magicMsg = STEG_MAGIC + msg + "\0";  //magicMsg formed by concatenation of STEG_MAGIC, msg & '\0'
  let stegLen = magicMsg.length;
  let binaryCode, pixel = 0, bitMask = 128;
  let result = this.unhide();		//check if image already have a hidden message

  if (result.msg) {     
    return {
      error: `STEG_MSG : ${this.id} : image already contains a hidden message`
    };
  }

  if (stegLen > dataLen) {	//check if image can accomodate the messsage
    return {
      error: `STEG_TOO_BIG : ${this.id} : message too big to be hidden in image`
    };
  }

  for (let i = 0; i < stegLen; i++) {
    for (let j = 0; j < 8; j++) {
      binaryCode = magicMsg.charCodeAt(i) & bitMask;  //extract binary code for each character of message
      if (binaryCode == 0 && (this.ppm.pixelBytes[pixel] & 1) == 1)
        this.ppm.pixelBytes[pixel] = this.ppm.pixelBytes[pixel] & ~1;
      else if (binaryCode > 0 && (this.ppm.pixelBytes[pixel] & 1) == 0)
        this.ppm.pixelBytes[pixel] = this.ppm.pixelBytes[pixel] | 1;
      bitMask = bitMask >> 1;   //right-shifting mask on each iteration
      pixel++;
    }
    bitMask = 128;
  }
  return { ppm: new Ppm(this.ppm) };	//return new image with same headers and new pixelBytes
}


/** Return message hidden in this StegModule object.  Specifically, if
 *  an error occurs, then return an object with "error" property set
 *  to a string describing the error.  If everything is ok, then the
 *  return'd object should have a "msg" property set to the hidden
 *  message.  Note that the return'd message should not contain
 *  STEG_MAGIC or the terminating NUL '\0' character.
 *
 *  The function will detect the following errors:
 *
 *    STEG_NO_MSG:    The image contained in this Steg object does not
 *                    contain a hidden message; detected by not
 *                    seeing this Steg object's underlying image pixel
 *                    bytes starting with a hidden STEG_MAGIC
 *                    string.
 *    STEG_BAD_MSG:   A bad message was decoded (the NUL-terminator
 *                    was not found).
 *
 * Each error message must start with the above IDs (STEG_NO_MSG, etc).
 */
StegModule.prototype.unhide = function () {
  //TODO
  let dataLen = this.ppm.pixelBytes.length;
  let charCode = "";
  let message = "";
  let isBadMsg = true;				//flag to check if message is corrupted (ends without '\0')
  for (let i = 0; i <= dataLen; i += 8) {
    for (let j = i; j < i + 8; j++) {
      if (this.ppm.pixelBytes[j] % 2 == 0) {
        charCode = charCode + 0;
      }
      else {
        charCode = charCode + 1;
      }
    }
    let currentChar = String.fromCharCode(parseInt(charCode, 2));
    message = message + String.fromCharCode(parseInt(charCode, 2));
    if (currentChar == '\0')			//exit once null terminator is encountered
    {
      isBadMsg = false;
      break;
    }
    charCode = "";
  }
  let magicLen = STEG_MAGIC.length;
  let magicStr = message.slice(0, magicLen);

  //   console.log("isBasMSG "+isBadMsg+" charcode"+ message.slice(message.length-1)+"last"+message.slice(-1));

  if (isBadMsg || message.slice(message.length - 1) != '\0') {
    return {
      error: `STEG_BAD_MSG : ${this.id} : bad message`
    };
  }
  if (magicStr == STEG_MAGIC) {
    return {
      msg: message.slice(magicLen, message.length-1)		//return message after removing STEG_MAGIC prefix
    };
  }
  else {
    return {
      error: `STEG_NO_MSG : ${this.id} : image does not have a message`
    };
  }
}


module.exports = StegModule;
