'use strict';

const Ppm = require('./ppm');

const assert = require('assert');
const os = require('os');
const fs = require('fs');

const path = require('path');
//TODO: add require()'s as necessary
const { promisify } = require('util'); //destructuring
const exec = promisify(require('child_process').exec);
const fsUnLink = promisify(fs.unlink);
const mongo = require('mongodb').MongoClient;
const readFileAsync = promisify(fs.readFile);




/** This module provides an interface for storing, retrieving and
 *  querying images from a database. An image is uniquely identified
 *  by two non-empty strings:
 *
 *    Group: a string which does not contain any NUL ('\0') 
 *           characters.
 *    Name:  a string which does not contain any '/' or NUL
 *           characters.
 *
 *  Note that the image identification does not include the type of
 *  image.  So two images with different types are regarded as
 *  identical iff they have the same group and name.
 *  
 *  Error Handling: If a function detects an error with a defined
 *  error code, then it must return a rejected promise rejected with
 *  an object containing the following two properties:
 *
 *    errorCode: the error code
 *    message:   an error message which gives details about the error.
 *
 *  If a function detects an error without a defined error code, then
 *  it may reject with an object as above (using a distinct error
 *  code), or it may reject with a JavaScript Error object as
 *  appropriate.
 */

function ImgStore(client, db) { //TODO: add arguments as necessary
  //TODO
  this.client = client;
  this.db = db;
}

ImgStore.prototype.close = close;
ImgStore.prototype.get = get;
ImgStore.prototype.list = list;
ImgStore.prototype.meta = meta;
ImgStore.prototype.put = put;

/** Factory function for creating a new img-store.
 */
async function newImgStore() {
  //TODO
  const client = await mongo.connect(MONGO_URL);
  const db = client.db(DB_NAME);
  return new ImgStore(client, db); //provide suitable arguments
}
module.exports = newImgStore;


/** URL for database images on mongodb server running on default port
 *  on localhost
 */
const MONGO_URL = 'mongodb://localhost:27017';
const DB_NAME = 'images';
const IMAGES_TABLE = 'imageInfo';


//List of permitted image types.
const IMG_TYPES = [
  'ppm',
  'png'
];


/** Release all resources held by this image store.  Specifically,
 *  close any database connections.
 */
async function close() {
  //TODO
  this.client.close();
}

/** Retrieve image specified by group and name.  Specifically, return
 *  a promise which resolves to a Uint8Array containing the bytes of
 *  the image formatted for image format type.
 *
 *  Defined Error Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 *    BAD_NAME:    name is invalid (contains a '/' or NUL-character).
 *    BAD_TYPE:    type is not one of the supported image types.
 *    NOT_FOUND:   there is no stored image for name under group.
 */
async function get(group, name, type) {
  //TODO: replace dummy return value
  if (isBadGroup(group))
    throw isBadGroup(group);

  else if (isBadName(name))
    throw isBadName(name);

  else if (isBadType(type))
    throw isBadType(type);

  let decodedBuffer;
  let decodedUint8Array;
  let projection;
  const missing = [];

  const dbTable = this.db.collection(IMAGES_TABLE);
  let filter = {
    group: group,
    name: name
  };
  if (type == 'png') {
    projection = {
      dataBytesPng: 1,
      _id: 0
    };
    const ret = await dbTable.find(filter).project(projection);
    let retImgs = await ret.toArray();
    if (retImgs.length === 0) {
      missing.push(name);
    }
    if (missing.length > 0) {
      const strMissing = missing.map((u) => JSON.stringify(u)).join(', ');
      throw new ImgError('NOT_FOUND', `image ${strMissing} not found`);
    }
    decodedUint8Array = new Uint8Array(retImgs[0].dataBytesPng.buffer); // (Uint8Array)
  }

  if (type == 'ppm') {
    projection = {
      dataBytesPpm: 1,
      _id: 0
    };
    const ret = await dbTable.find(filter).project(projection);
    let retImgs = await ret.toArray();
    if (retImgs.length === 0) {
      missing.push(name);
    }
    if (missing.length > 0) {
      const strMissing = missing.map((u) => JSON.stringify(u)).join(', ');
      throw new ImgError('NOT_FOUND', `image ${strMissing} not found`);
    }

    let imgBytes = retImgs[0].dataBytesPpm;
    decodedUint8Array = new Uint8Array(imgBytes.buffer); // (Uint8Array)
  }
  return decodedUint8Array;
}

/** Return promise which resolves to an array containing the names of
 *  all images stored under group.  The resolved value should be an
 *  empty array if there are no images stored under group.
 *
 *  The implementation of this function must not read the actual image
 *  bytes from the database.
 *
 *  Defined Errors Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 */
async function list(group) {
  //TODO: replace dummy return value

  if (isBadGroup(group))
    throw isBadGroup(group);

  const dbTable = this.db.collection(IMAGES_TABLE);
  let filter = {
    group: group
  };
  let projection = {
    name: 1,
    _id: 0
  };
  const ret = await dbTable.find(filter).project(projection);
  let retImgNames = await ret.toArray();
  retImgNames = retImgNames.map(value => value.name);
  return retImgNames;
}

/** Return promise which resolves to an object containing
 *  meta-information for the image specified by group and name.
 *
 *  The return'd object must contain the following properties:
 *
 *    width:         a number giving the width of the image in pixels.
 *    height:        a number giving the height of the image in pixels.
 *    maxNColors:    a number giving the max # of colors per pixel.
 *    nHeaderBytes:  a number giving the number of bytes in the 
 *                   image header.
 *    creationTime:  the time the image was stored.  This must be
 *                   a number giving the number of milliseconds which 
 *                   have expired since 1970-01-01T00:00:00Z.
 *
 *  The implementation of this function must not read the actual image
 *  bytes from the database.
 *
 *  Defined Errors Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 *    BAD_NAME:    name is invalid (contains a '/' or NUL-character).
 *    NOT_FOUND:   there is no stored image for name under group.
 */
async function meta(group, name) {
  //TODO: replace dummy return value

  if (isBadGroup(group))
    throw isBadGroup(group);

  else if (isBadName(name))
    throw isBadName(name);

  const missing = [];
  const dbTable = this.db.collection(IMAGES_TABLE);
  const filter = {
    group: group,
    name: name
  };
  const projection = {
    metaInfo: 1,
    _id: 0
  };
  const ret = await dbTable.find(filter).project(projection);
  const retImgs = await ret.toArray();
  if (retImgs.length === 0) {
    missing.push(name);
  }
  if (missing.length > 0) {
    const strMissing = missing.map((u) => JSON.stringify(u)).join(', ');
    throw new ImgError('NOT_FOUND', `image '${strMissing}' not found under group '${group}'`);
  }
  let metaInfo = retImgs[0].metaInfo;
  return ['width', 'height', 'maxNColors', 'nHeaderBytes', 'creationTime']
    .reduce((acc, e) => { acc[e] = metaInfo[e]; return acc; }, {});
}

/** Store the image specified by imgPath in the database under the
 *  specified group with name specified by the base-name of imgPath
 *  (without the extension).  The resolution of the return'd promise
 *  is undefined.
 *
 *  Defined Error Codes:
 *
 *    BAD_GROUP:   group is invalid (contains a NUL-character).
 *    BAD_FORMAT:  the contents of the file specified by imgPath does 
 *                 not satisfy the image format implied by its extension. 
 *    BAD_TYPE:    the extension for imgPath is not a supported type
 *    EXISTS:      the database already contains an image under group
 *                 with name specified by the base-name of imgPath
 *                 (without the extension). 
 *    NOT_FOUND:   the path imgPath does not exist.
 * 
 */
async function put(group, imgPath) {
  //TODO
  const dbTable = this.db.collection(IMAGES_TABLE);
  const dups = [];

  let imgBufferPng, imgBufferPpm;
  let encodedPpmString, encodedPngString;

  if (isBadGroup(group))
    throw isBadGroup(group);

  else if (isBadExt(imgPath))
    throw isBadExt(imgPath);

  else if (isBadPath(imgPath))
    throw isBadPath(imgPath);

  let imgName = pathToNameExt(imgPath)[0];
  let imgType = pathToNameExt(imgPath)[1];
  let imgId = toImgId(group, imgName);

  if (imgType == "png") {
    try {
      imgBufferPng = await readFileAsync(imgPath);
      const dirPath = os.tmpdir();
      let destPath = dirPath + '/tmp.ppm';
      const cmd = 'convert ' + imgPath + ' ' + destPath;
      const { stdout, stderr } = await exec(cmd); //destructuring
      imgBufferPpm = await readFileAsync(destPath);
      await fsUnLink(destPath);
    }

    catch (err) {
      throw new ImgError('BAD_FORMAT', `bad image format`);
    }
  }

  if (imgType == 'ppm') {
    try {
      imgBufferPpm = await readFileAsync(imgPath);
      const dirPath = os.tmpdir();
      let destPath = dirPath + '/tmp.png';
      const cmd = 'convert ' + imgPath + ' ' + destPath;
      const { stdout, stderr } = await exec(cmd); //destructuring
      imgBufferPng = await readFileAsync(destPath);
      await fsUnLink(destPath);
    }
    catch (err) {
      throw new ImgError('BAD_FORMAT', `bad image format`);
    }
  }

  let uInt8Array = new Uint8Array(imgBufferPpm);
  let imgInfo = new Ppm(imgId, uInt8Array);
  let imgDocument = {
    _id: imgId,
    id: imgId,
    group: group,
    name: imgName,
    type: imgType,
    metaInfo: {
      width: imgInfo.width,
      height: imgInfo.height,
      maxNColors: imgInfo.maxNColors,
      nHeaderBytes: imgInfo.nHeaderBytes,
      creationTime: Date.now()
    },
    dataBytesPng: imgBufferPng,
    dataBytesPpm: imgBufferPpm
  };
  try {
    const ret = await dbTable.insertOne(imgDocument);
    assert(ret.insertedId === imgDocument.id);
  }

  catch (err) {
    if (isDuplicateError(err)) {
      dups.push(imgId);
    }
    else if (typeof err === 'object' && err.errorCode && err.message) {
      console.error(`${err.errorCode}: ${err.message}`);
    }
    else {
      throw err;
    }
  }
  if (dups.length > 0) {
    throw new ImgError('EXISTS', `image with id ${dups.join(', ')} already exist`);
  }
  return;
}



//Utility functions

const NAME_DELIM = '/', TYPE_DELIM = '.';

/** Form id for image from group, name and optional type. */
function toImgId(group, name, type) {
  let v = `${group}${NAME_DELIM}${name}`;
  if (type) v += `${TYPE_DELIM}${type}`
  return v;
}

/** Given imgId of the form group/name return [group, name]. */
function fromImgId(imgId) {
  const nameIndex = imgId.lastIndexOf(NAME_DELIM);
  assert(nameIndex > 0);
  return [imgId.substr(0, nameIndex), imgId.substr(nameIndex + 1)];
}

/** Given a image path imgPath, return [ name, ext ]. */
function pathToNameExt(imgPath) {
  const typeDelimIndex = imgPath.lastIndexOf(TYPE_DELIM);
  const ext = imgPath.substr(typeDelimIndex + 1);
  const name = path.basename(imgPath.substr(0, typeDelimIndex));
  return [name, ext];
}

//Error utility functions

function isBadGroup(group) {
  return (group.trim().length === 0 || group.indexOf('\0') >= 0) &&
    new ImgError('BAD_GROUP', `bad image group ${group}`);
}

function isBadName(name) {
  return (name.trim().length === 0 ||
    name.indexOf('\0') >= 0 || name.indexOf('/') >= 0) &&
    new ImgError('BAD_NAME', `bad image name '${name}'`);
}

function isBadExt(imgPath) {
  const lastDotIndex = imgPath.lastIndexOf('.');
  const type = (lastDotIndex < 0) ? '' : imgPath.substr(lastDotIndex + 1);
  return IMG_TYPES.indexOf(type) < 0 &&
    new ImgError('BAD_TYPE', `bad image type '${type}' in path ${imgPath}`);
}

function isBadPath(path) {
  return !fs.existsSync(path) &&
    new ImgError('NOT_FOUND', `file ${path} not found`);
}

function isBadType(type) {
  return IMG_TYPES.indexOf(type) < 0 &&
    new ImgError('BAD_TYPE', `bad image type '${type}'`);
}

function isDuplicateError(err) {
  return (err.code === 11000);
}

/** Build an image error object using errorCode code and error 
 *  message msg. 
 */
function ImgError(code, msg) {
  this.errorCode = code;
  this.message = msg;
}

