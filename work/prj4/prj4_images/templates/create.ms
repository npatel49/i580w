<!DOCTYPE html>
<html>
  <head>
    <title>Hide a message in Image</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    <h1>Hide a message</h1>
    <ul>
      {{#errors}}
        <li class="error">{{.}}</li>
      {{/errors}}
    </ul>
    <form method="POST" action="{{{base}}}/hide.html">
       <p>
	 Please fill in one or more of the following fields
        </p>
	    <label>
	    <span class="label">
	      * Message :
	    </span>
	    <input name="msg" type="text" value="">
	    </label>
	    <br/>
	    {{#errorMessage}}
	      <span class="error">{{errorMessage}}</span><br/>
	    {{/errorMessage}}
      <input name="submit" type="submit" value="hide" class="control">
    </form> 
    {{>footer}}
  </body>
</html>
