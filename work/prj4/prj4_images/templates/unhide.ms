<!DOCTYPE html>
<html>
  <head>
    <title>Hide Service</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
      {{>footer}}
    <h1>Hide Service</h1>
    <ul>
      {{#errors}}
        <li class="error">{{.}}</li>
            <br/>
      {{/errors}}
    </ul>

    <form method="POST" action="{{{base}}}/doUnHide.html">        
    {{#fields}}
        <div>
              {{#isImg}}
                    <label>
                  <span class="label"> * {{friendlyName}} : </span>
              {{/isImg}}
               

            {{#imageUrls}}
                <div class="container">    
                <label  class="inline">
                    <input class ="imgSelect" type="radio" name="choice" value={{imgName}} />
                    <img src="{{imgUrl}}">
                        <div class="imgName">
                        <p>{{imgName}}</p>
                        </div>
                </label>       
                </div>
            {{/imageUrls}}


                    </label>   
            {{#errorMessage}}
                     <br/>
	                 <span class="error">{{errorMessage}}</span><br/>
	          {{/errorMessage}}
        </div>  
    {{/fields}}      
         <div>
            <button name="submit" type="submit" value="Unhide" class="control button" style="vertical-align:middle">
            <span>Hide </span>
         </div>
    </form>

  </body>
</html>
