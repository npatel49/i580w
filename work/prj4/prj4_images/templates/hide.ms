<!DOCTYPE html>
<html>
  <head>
    <title>Hide Service</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
      {{>footer}}
    <h1>Hide Service</h1>
    <ul>
      {{#errors}}
        <li class="error">{{.}}</li>
            <br/>
      {{/errors}}
    </ul>

    <form method="POST" enctype="multipart/form-data" action="{{{base}}}/doHide.html">        
    {{#fields}}
        <div>
                <label>
                  <span class="label"> * {{friendlyName}} : </span>

            {{#imageUrls}}
                <div class="container">    
                <label  class="inline">
                    <input class ="imgSelect" type="radio" name="choice" value={{imgName}} />
                    <img src="{{imgUrl}}">
                        <div class="imgName">
                        <p>{{imgName}}</p>
                        </div>
                </label>       
                </div>
            {{/imageUrls}}

            {{^imageUrls}}
                    <input name="{{name}}" type="{{type}}" value="{{value}}">
            {{/imageUrls}}
                    </label>   
            {{#errorMessage}}
                     <br/>
	                 <span class="error">{{errorMessage}}</span><br/>
	        {{/errorMessage}}
        </div>  
    {{/fields}}      
         <div>
            <button name="submit" type="submit" value="Hide" class="control button" style="vertical-align:middle">
            <span>Hide </span>
         </div>
    </form>

  </body>
</html>
