<!DOCTYPE html>
<html>
  <head>
    <title>Hide Success</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
      {{>footer}}
    <h1>Hide Success</h1>
    <ul>
      {{#errors}}
        <li class="error">{{.}}</li>
      {{/errors}}
    </ul>
    
    {{#users}}
        <ul>
            <li>Image <b>"{{.}}"</b> hidden in "steg" group</li>      
        </ul>	
	  {{/users}}

  </body>
</html>
