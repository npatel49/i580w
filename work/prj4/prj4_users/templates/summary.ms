<!DOCTYPE html>
<html>
  <head>
    <title>Users List</title>
    <link rel="stylesheet" href="{{{base}}}/css/style.css">
  </head>
  <body>
    <h1>Users List</h1>
    <p class="error">{{msg}}</p>
    {{#users}}
    <div class="container">
      <img src="{{imgUrl}}">
      <a href="{{id}}.html">details</a>
    </div>
    {{/users}}
    {{>footer}}
  </body>
</html>
